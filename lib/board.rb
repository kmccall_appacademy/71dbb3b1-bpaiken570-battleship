class Board
  attr_accessor :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(empty_grid = Board.default_grid)
    @grid = empty_grid
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    pos ? self[pos].nil? : grid.flatten.all? { |e| e != :s }
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise 'board full' if full?
    pos = random_pos
    pos = random_pos until empty?(pos)
    self[pos] = :s
  end

  def random_pos
    row = rand(grid.length)
    col = rand(grid[0].length)
    [row, col]
  end

  def won?
    grid.flatten.none? { |e| e == :s }
  end

  def display
    print "\t"
    10.times { |col| print " #{col}\t" }
    @grid.each_index do |row|
      print "\n#{row}\t"
      @grid[0].each_index do |col|
        print_pos([row, col])
        print "\t"
      end
    end
  end

  def print_pos(pos)
    if self[pos].nil? || self[pos] == :s
      print '~~~'
    else
      print " #{self[pos]} "
    end
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end
end
