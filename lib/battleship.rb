require_relative 'player'
require_relative 'board'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new, board = Board.new)
    @player = player
    @board = board
  end

  def attack(pos)
    board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end

  def play
    5.times { board.place_random_ship }
    until game_over?
      display_status
      play_turn
    end
    print "\nYou sunk all ships!"
  end

  def display_status
    board.display
    print "\nThere are #{board.count} ships remaining\n"
  end
end

if __FILE__ == $PROGRAM_NAME
  game = BattleshipGame.new
  game.play
end
